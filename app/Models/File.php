<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['name', 'path', 'article_id', 'uploaded_by'];
    protected $table    = 'file';
}
