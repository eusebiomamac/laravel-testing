<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    protected $fillable = ['title', 'content', 'created_by'];
    protected $table    = 'article';

    public static function getArticleDetails($article_id) {
        $query = DB::select("
            SELECT
                article.id,
                article.title,
                article.content,
                article.created_at,
                file.path,
                file.name
            FROM article
            LEFT JOIN file ON file.article_id = article.id
            WHERE article.id = ?
        ", array($article_id));

        return !empty($query) ? $query[0] : false;
    }

    public static function getAllArticles($paginate = array()) {
        $query = "
            SELECT
                article.id,
                article.title,
                article.content,
                article.created_at,
                file.path,
                file.name
            FROM article
            LEFT JOIN file ON file.article_id = article.id
            ORDER BY article.created_at DESC
        ";

        if (!empty($paginate))
            $query .= " LIMIT ".$paginate['offset'].",".$paginate['record'];

        return DB::select($query);
    }

    public static function count() {
        $query =  DB::select("
            SELECT count(*) as value FROM article
        ");

        return !empty($query) ? $query[0]->value : 0;
    }
}
