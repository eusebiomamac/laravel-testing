<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;

class ArticleController extends Controller
{
    public function clientArchive(Request $request) {
        try {

            $record = 5;
            $page   = $request->has('page') ? $request->get('page') : 1;
            $offset = ($page-1) * $record;
            $params = array('record' => $record, 'offset' => $offset);

            $articles = Article::getAllArticles($params);
            $articles = new LengthAwarePaginator($articles, Article::count(), $record, $page, [
                'path'  => $request->url(),
                'query' => $request->query(),
            ]);

            return view('article.archive')
                ->with('articles', $articles);

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function clientIndex(Request $request) {
        try {

            $params = array('record' => 5, 'offset' => 0);
            $articles = Article::getAllArticles($params);

            return view('article.client_index')
                ->with('articles', $articles);

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function adminIndex() {
        try {

            $articles = Article::getAllArticles();

            return view('article.admin_index')
                ->with('articles', $articles);

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function viewArticle($id, Redirector $redirect) {
        try {

            $article = Article::getArticleDetails($id);

            if (!$article)
                return redirect()->route('client-list');

            return view('article.view')
                ->with('article', $article);

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function newArcticle() {
        try {

           return view('article.new');

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function createArcticle(Request $request, Redirector $redirect) {
        try {

            $file       = $request->file('image');
            $content    = $request->input('content');
            $title      = $request->input('title');
            $user_id    = Auth::id();

            $article = Article::create(array(
                'title'       => $title,
                'content'     => $content,
                'created_by'  => $user_id
            ));

            File::create(array(
                'name'        => $file ? $file->getClientOriginalName()   : '',
                'path'        => $file ? $file->store('images', 'public') : '',
                'uploaded_by' => $user_id,
                'article_id'  => $article->id
            ));

           return redirect()->route('admin-article-edit', array('id' => $article->id));

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function editArcticle($id, Redirector $redirect) {
        try {

            $article = Article::getArticleDetails($id);

            if (!$article)
                return redirect()->route('admin-list');

            return view('article.edit')
                ->with('article', $article);

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }

    public function updateArcticle(Request $request, Redirector $redirect) {
        try {

            if (!$request->input('article_id'))
                return redirect()->route('admin-list');

            $file       = $request->file('image');
            $article_id = $request->input('article_id');
            $content    = $request->input('content');
            $title      = $request->input('title');
            $user_id    = Auth::id();
            $file_data  = array();


            if ($file)
                $file_data = array(
                    'name'  => $file->getClientOriginalName(),
                    'path'  => $file->store('images', 'public'),
                );

            File::where('article_id', $article_id)->update($file_data);

            Article::find($article_id)->update(array(
                'title'    => $title,
                'content'  => $content,
            ));

           return redirect()->route('admin-article-edit', array('id' => $article_id));

        } catch (Exception $e) {
            return redirect()->route('client-list');
        }
    }
}
