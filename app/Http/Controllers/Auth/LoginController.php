<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request, Redirector $redirect) {
        $this->validate($request,
            array(
                'auth_id'   => 'required|string',
                'password'  => 'required|string',
            ),
            array(
                'auth_id.required'  => 'User ID or Password is required',
                'password.required' => 'Password is required',
            )
        );

        if (Auth::attempt(array('auth_id' => $request->input('auth_id'), 'password' => $request->input('password')))) {
            return redirect()->route('admin-list');
        }

        return redirect()->back()
            ->withErrors([
                'password' => 'Invalid credentials'
            ]);
    }
}
