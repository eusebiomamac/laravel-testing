<?php

namespace App\Utils;

class Helper
{
    public static function checkPrefix($string) {
        return strpos(Request()->route()->getPrefix(), $string) ? 1 : 0;
    }
}
