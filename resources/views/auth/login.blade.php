@extends('layouts.main')

@section('content')

<!--start l-contents-->
<div class="l-container u-clear">
    <!--start l-main-->
    <main class="l-main js-main">
        <div class="l-main-block"></div>
        <form method="POST" action="{{ route('login') }}" class="form">
            {{ csrf_field() }}
            <label for="auth_id" class="form-title">USER ID</label>
            <input type="text" id="auth_id" name="auth_id" class="input input-text"  value="{{ old('auth_id') }}">
            <label for="password" class="form-title">PASSWORD</label>
            <input type="password" id="password" name="password" class="input input-text">
            @if ($errors->has('auth_id') || $errors->has('password'))
                <div class="error" style="
                    width: 100%;
                    text-align: center;
                    color: red;
                    margin-top: 20px;
                ">
                    <span>Error</span>
                </div>
            @endif
            <label for="submit" class="form-button">
                <div class="button">
                    <p class="button-text">Login</p>
                </div>
            </label>
            <input type="submit" id="submit" class="input input-submit">
        </form>
    </main>
    <!--end l-main-->
</div>
<!--end l-contents-->
@endsection
