@extends('layouts.main')

@section('content')
   <!--start l-contents-->
<div class="l-container u-clear">
    <!--start l-main-->
    <main class="l-main js-main">
        <div class="l-main-block"></div>
        <a href="{{ route('admin-article-new') }}" class="l-main-button" style="text-decoration: none;">
            <div class="button">
                <p class="button-text">New Article</p>
            </div>
        </a>

        @if (!count($articles))
            <div style="text-align: center; margin-top: 5%;">
                <small class="copyright" style="font-size: 11px;">No Articles Found</small>
            </div>
        @endif

        <ul class="archive archive-admin">
            @foreach($articles as $article)
                <li class="archive-item">
                    <a href="{{ route('admin-article-edit', $article->id) }}" class="post-article">
                        <time class="post-article-date" datetime="{{ $article->created_at }}">{{ date("F j, Y", strtotime($article->created_at)) }}</time>
                        <h1 class="post-article-title">{{ ucfirst($article->title) }}</h1>
                    </a>
                </li>
            @endforeach
        </ul>
    </main>
    <!--end l-main-->
</div>
<!--end l-contents-->
@endsection
