@extends('layouts.main')

@section('content')
<!--start l-contents-->
<div class="l-container u-clear">
    <!--start l-main-->
    <main class="l-main js-main">
        <div class="l-main-block"></div>
        <form action="{{ route('admin-article-create') }}"  method="POST" class="form" enctype="multipart/form-data">
            {{ csrf_field() }}
            <label for="image" class="form-title">EYE CATCH IMAGE
                <div class="form-file u-clear">
                    <span id="file-name"></span>
                    <span class="form-file-button">UPLOAD</span>
                </div>
            </label>
            <input type="file" id="image" name="image" class="input input-image">
            <label for="title" class="form-title">TITLE</label>
            <div class="form-body">
                <input type="text" id="title" name="title" class="input input-text">
            </div>
            <label for="contents" class="form-title">CONTENTS</label>
            <div class="form-textarea">
                <textarea name="content" id="inquiry" cols="30" rows="10" class="input input-contents"></textarea>
            </div>
            <div class="error" style="
                width: 100%;
                text-align: center;
                color: red;
                margin-top: 20px;
                display: none;
            ">
                <span>Error</span>
            </div>
            <label for="submit" class="form-button">
                <div class="button">
                    <p class="button-text">Submit</p>
                </div>
            </label>
            <input type="submit" id="submit" class="input input-submit">
            <a href="{{ route('admin-list') }}" class="form-button" style="text-decoration: none;">
                <div class="button">
                    <p class="button-text">Back</p>
                </div>
            </a>
        </form>
    </main>
    <!--end l-main-->
</div>
<script>
    (function($) {
        $(document).ready(() => {

            const form       = $('.form')
            const error      = $('.error')
            const file_input = $('#image')
            const file_name  = $('#file-name')

            file_input.on('change', (event) => {
                event.preventDefault();

                const [ file ] = event.target.files
                file_name.text(file ? file.name : '')
            })

            form.submit((event) => {
                if (form.serializeArray().find(field => !field.value)) {
                    error.show()
                    return event.preventDefault()
                }

                error.hide()
            })
        })
    }($))
</script>
<!--end l-contents-->
@endsection
