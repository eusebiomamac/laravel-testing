@extends('layouts.main')

@section('content')
 <!--start l-contents-->
<div class="l-container u-clear">
    <!--start l-main-->
    <main class="l-main js-main">
        <div class="l-main-block"></div>
        <div class="archive">
            <ul class="archive-list">

                @foreach($articles as $article)
                    <li class="archive-item">
                        <article class="card">
                            <a href="{{ route('client-arcticle-view', $article->id) }}"  class="card-link">
                                <img src="{{ asset('storage/'.$article->path) }}" alt="" class="card-image">
                                <div class="card-bottom">
                                    <h1 class="card-title">{{ $article->title }}</h1>
                                    <time class="card-date" datetime="{{ $article->created_at }}">
                                        {{ date("F j, Y", strtotime($article->created_at)) }}
                                    </time>
                                </div>
                            </a>
                        </article>
                    </li>
                @endforeach
            </ul>
        </div>
        <a href="{{ route('client-list-archive') }}" class="archive-button">
            <div class="button">
                <p class="button-text">More</p>
            </div>
        </a>
    </main>
    <!--end l-main-->
</div>
<!--end l-contents-->
@endsection
