@extends('layouts.main')

@section('content')
<!--start l-contents-->
<div class="l-container u-clear">
    <!--start l-main-->
    <main class="l-main js-main">
        <div class="l-main-block"></div>
        <div class="page-number">
            Page <span>{{ $articles->currentPage() }}/{{ $articles->lastPage() }}</span>
        </div>
        <div class="archive">
            <ul class="archive-list">
                @foreach($articles as $article)
                    <li class="archive-item">
                        <article class="card">
                            <a href="{{ route('client-arcticle-view', $article->id) }}"  class="card-link">
                                <img src="{{ asset('storage/'.$article->path) }}" alt="" class="card-image">
                                <div class="card-bottom">
                                    <h1 class="card-title">{{ $article->title }}</h1>
                                    <time class="card-date" datetime="{{ $article->created_at }}">
                                        {{ date("F j, Y", strtotime($article->created_at)) }}
                                    </time>
                                </div>
                            </a>
                        </article>
                    </li>
                @endforeach
            </ul>
        </div>
        <div>{{ $articles->links() }}</div>


    </main>
<!--end l-main-->
</div>
<!--end l-contents-->
<style>
    .pagination {
        letter-spacing: 0;
        font-size: 0;
        text-align: center;
        margin-top: 40px;
        position: relative;
    }

    .pagination li {
        display: inline-block;
        font-size: 20px;
        text-align: center;
        padding-top: 5px;
        width: 40px;
        height: 40px;
        background-color: #333;
        color: #fff;
        font-family: Helvetica, Arial, メイリオ, Meiryo, sans-serif;
        vertical-align: top;
        margin-left: 10px;
    }
</style>
<script>
    (function($) {
        const paginate = $('.pagination')

        $(document).ready(() => {
            paginate.addClass('paginate').find('li').each(function(index) {

                $(this)
                    .children('a')
                    .css('color', '#fff')

                if ($(this).hasClass('active'))
                    $(this).addClass('is-current')

                if ($(this).hasClass('disabled'))
                    $(this).addClass('is-disable')

                if (!index) {
                    $(this).children('span, a')
                        .addClass('paginate-prev-arrow')
                        .html('')
                        .css({
                            top: '65% !important',
                            left: '42.5%',
                            position: 'relative'
                        })
                }

                if (index === paginate.find('li').length - 1) {
                    $(this).children('span, a')
                        .addClass('paginate-next-arrow')
                        .html('')
                        .css({
                            top: '50%',
                            left: '58.2%',
                            position: 'relative'
                        })
                }
            })
        })
    }($))
</script>
@endsection
