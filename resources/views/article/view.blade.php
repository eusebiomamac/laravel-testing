@extends('layouts.main')

@section('content')
<!--start l-main-->
<main class="l-main js-main">
    <div class="l-main-block"></div>
    <div class="single">
        <img src="{{ asset('storage/'.$article->path) }}" alt="" class="single-image">
        <div class="l-container u-clear">
            <h1 class="single-title">{{ $article->title }}</h1>
            <time class="single-date" datetime="{{ $article->created_at }}">{{ date("F j, Y", strtotime($article->created_at)) }}</time>
            <p class="single-desc">{{ $article->content }}</p>
            <a class="single-button" href="{{ route('client-list') }}" style="text-decoration: none;">
                <div class="button">
                    <p class="button-text">Top</p>
                </div>
            </a>
        </div>
    </div>
</main>
<!--end l-main-->
@endsection
