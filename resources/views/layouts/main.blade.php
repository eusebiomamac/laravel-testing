<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>{{ config('app.name', 'Blog') }}</title>
    <meta name="description" content="sample text,sample tex">
    <meta name="keywords" content="word1,word2,word3">

     <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:title" content="">
    <meta property="og:description" content="sample text,sample tex">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:type" content="blog">
    <meta property="fb:admins" content="">
    <meta property="og:image" content="{{ asset('images/common/ogp.png') }}">
    <meta name="apple-mobile-web-app-title" content="">

    <!-- Styles -->
    <link rel="shortcut icon" href="{{ asset('images/common/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" href=" {{ asset('images/common/apple-touch-icon-precomposed.png') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('lib/modernizr.js') }}"></script>
    <script src="{{ asset('lib/jquery-3.1.1.min.js') }}"></script>
</head>
<body  id="js-body">
    <noscript class="for-no-js">Javascriptを有効にしてください。</noscript>
    <div class="for-old">お使いのOS・ブラウザでは、本サイトを適切に閲覧できない可能性があります。最新のブラウザをご利用ください。</div>
    <div id="app">

        @if (Helper::checkPrefix('admin'))
            @include('layouts.header.admin')
        @else
            @include('layouts.header.client')
        @endif

        @yield('content')
        @include('layouts.footer')
    </div>

    <!-- Custom Scripts -->
    <script src="{{ asset('js/lig/app.js') }}"></script>
</body>
</html>
