<header class="l-header l-header-admin js-header">
    <div class="l-header-top u-clear">
        <a class="l-header-logo" href="{{ route('admin-list') }}">
            <div class="logo">
                <img src="{{ asset('images/logo-admin.png') }}" width="138" height="28" alt="BLOG" />
            </div>
        </a>
        <div class="l-header-text">
            <p>ADMIN PAGE</p>
        </div>
    </div>
</header>