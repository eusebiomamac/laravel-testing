<!--start header-->
<header class="l-header  js-header">
    <div class="l-header-top u-clear">
        <a class="l-header-logo" href="{{ route('client-list') }}">
            <div class="logo ">
                <img src="{{ asset('images/logo.png') }}" width="253" height="28" alt="BLOG" />
            </div>
        </a>
        <div class="l-header-hamburger">
            <a href="#" class="hamburger js-hamburger ">
                <span class="hamburger-item"></span>
                <span class="hamburger-item"></span>
                <span class="hamburger-item"></span>
            </a>
        </div>
    </div>
</header>
<nav class="nav js-nav">
    <ul class="nav-list">
        <li class="nav-item">
            <a href="{{ route('client-list') }}" class="nav-link">Home</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('client-list-archive') }}" class="nav-link">Archive</a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin-list') }}" class="nav-link">Admin</a>
        </li>
    </ul>
</nav>
<!--end header-->