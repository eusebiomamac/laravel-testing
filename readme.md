## Steps

- Install dependencies using composer.
- Create a database base on the .env file .
- **Open Terminal**
- Navigate to project directory.
- *php artisan migrate*.
- *php artisan db:seed*.
- serve the project *php artisan serve --port=8080*.
- **Account Cred**
- user id: 100000
- password: password

