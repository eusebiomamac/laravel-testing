<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{

    public function run() {
        DB::table('users')->delete();
        User::create(array(
            'auth_id'  => '100000',
            'name'     => 'Eusebio Mamac',
            'email'    => 'eusebiomamac@gmail.com',
            'password' => Hash::make('password'),
        ));
    }
}
