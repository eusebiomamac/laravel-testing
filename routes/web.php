<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(array('middleware' => array('web')), function() {
    Route::get('/article/{id}',     array('uses' => 'ArticleController@viewArticle'))       ->name('client-arcticle-view');
    Route::get('/archive',          array('uses' => 'ArticleController@clientArchive'))     ->name('client-list-archive');
    Route::get('/',                 array('uses' => 'ArticleController@clientIndex'))       ->name('client-list');
});

Route::prefix('admin')->middleware(array('auth'))->group(function() {
    Route::get('/article/{id}',     array('uses' => 'ArticleController@editArcticle'))      ->name('admin-article-edit');
    Route::get('/article',          array('uses' => 'ArticleController@newArcticle'))       ->name('admin-article-new');
    Route::get('/',                 array('uses' => 'ArticleController@adminIndex'))        ->name('admin-list');

    Route::post('/article',         array('uses' => 'ArticleController@createArcticle'))    ->name('admin-article-create');
    Route::post('/article/update',  array('uses' => 'ArticleController@updateArcticle'))    ->name('admin-article-update');
});